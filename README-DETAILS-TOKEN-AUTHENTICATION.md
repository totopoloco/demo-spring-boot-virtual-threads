# Introduction

If we are done with the form authentication and it serves the purpose of getting your project up and running then that is fine, but if you need to expose a REST API then read on.

The first thing we will do is switch GIT branches by issuing the following command.

```
git fetch && git branch -r
```

This will show you the list of branches. Then switch to the branch

```
git checkout with-security-II
```

# Authentication
The only configuration needed in Keycloak is regarding the token authentication. 

If you remember from the previous readme file we mentioned the following

> Go back to "Settings" tab and scroll down to "Authentication flow" verify that Direct access grants is not checked
> We will need to check it if we use JWT token based authentication.

Now with need to the opposite, please check the checkbox

We can proceed to authenticate first to get a JWT token by sending a POST request (x-www-form-urlencoded) to the following URL:

```
http://localhost:8080/realms/oauth2-demo-realm/protocol/openid-connect/token
```

The following key/value pairs are required

| Key           | Value             |
|:------------- |:----------------- |
| client_id     | [client-id]       |
| grant_type    | password          |
| client_secret | [client-secret]   |
| scope         | openid            |
| username      | [username]        |
| password      | [password]        |

In this example the client_id is 

oauth2-demo-thymeleaf-client

Also, client_secret is the link between the spring boot application and keycloak (see the previous readme file for where to find this secret).

# Access the secure endpoint

This demo is configured to access the following pattern without a need for authentication

GET/POST http://localhost:8060/api/countries_fetch

For all the other operations such as 

POST http://localhost:8060/api/countries_admin

A token is required in the header, if you are using CURL the format would be as follows:

```
curl -v {url} -H "accept: application/json" -H "Authorization: Bearer {token}"
```

However, for easier visualisation of the request/response, Postman can be used.
