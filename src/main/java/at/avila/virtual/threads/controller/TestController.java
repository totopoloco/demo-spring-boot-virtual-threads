package at.avila.virtual.threads.controller;

import at.avila.virtual.threads.configuration.CurrentThreadService;
import io.micrometer.observation.annotation.Observed;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@Observed
@Hidden
public class TestController {
  private final CurrentThreadService currentThreadService;

  public TestController(CurrentThreadService currentThreadService) {
    this.currentThreadService = currentThreadService;
  }

  @GetMapping("/test")
  public String currentThread() {
    final String string = this.currentThreadService.currentThread(Thread.currentThread().getName());
    log.info("Current thread: {}", string);
    return string;
  }

}