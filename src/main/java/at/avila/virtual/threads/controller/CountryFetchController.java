package at.avila.virtual.threads.controller;

import at.avila.virtual.threads.db.CountryService;
import at.avila.virtual.threads.model.Country;
import io.micrometer.observation.annotation.Observed;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@Observed
@CacheConfig(cacheNames = "myCountryItems")
public class CountryFetchController implements CountriesFetchApi {

  private final CountryService countryService;

  public CountryFetchController(final CountryService countryService) {
    this.countryService = countryService;
  }

  @Override
  @Cacheable
  public ResponseEntity<List<Country>> getAllCountries() {
    return ResponseEntity.ok(this.countryService.getAll());
  }

  @Override
  @Cacheable(cacheNames = "myCountryItems", key = "#country")
  public ResponseEntity<Country> getCountries(Optional<Country> country) {

    if (country.isEmpty()) {
      return ResponseEntity.badRequest().build();
    }

    Country countryObj = country.get();

    /*
     * Tries to get the country by its id, if it is not present,
     * tries to get it by its name.
     * If not found in the database, then return a not found response.
     */
    return countryObj.getId()
        .map(this::getCountryById)
        .orElseGet(() -> getCountryByName(countryObj));
  }

  private ResponseEntity<Country> getCountryById(Integer integer) {
    return this.countryService.getCountryById(integer)
        .map(ResponseEntity::ok)
        .orElseGet(() ->
            ResponseEntity.notFound().build());
  }

  /**
   * Tries to get the country by its name.
   *
   * @param countryObj the country object.
   * @return the response entity.
   */
  private ResponseEntity<Country> getCountryByName(Country countryObj) {
    return countryObj.getEnglishName()
        .map(this::fetchByEnglishName)
        .orElseGet(() -> ResponseEntity.badRequest().build());
  }

  private ResponseEntity<Country> fetchByEnglishName(String englName) {
    return this.countryService.getCountryByName(englName)
        .map(ResponseEntity::ok)
        .orElseGet(
            () -> ResponseEntity.notFound().build());
  }

  @CacheEvict(allEntries = true)
  @Scheduled(cron = "#{cacheManagementConfiguration.cleanCacheCron}")
  public void cleanCache() {

    if (!log.isDebugEnabled()) {
      return;
    }

    log.debug("Cleaned cached at: <{}>.", Instant.now());
  }


  @PreDestroy
  public void destroy() {
    log.info("Destroying bean of type <{}> at <{}>.", this.getClass().getSimpleName(), Instant.now());
  }

  @PostConstruct
  public void postConstruct() {
    log.info("Constructing bean of type <{}> at <{}>", this.getClass().getSimpleName(), Instant.now());
  }

}
