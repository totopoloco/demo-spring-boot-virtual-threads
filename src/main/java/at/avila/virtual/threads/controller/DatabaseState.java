package at.avila.virtual.threads.controller;

/**
 * Keeps track of the database state.
 */
public enum DatabaseState {
  /**
   * When the state is UPDATED, don't proceed with further updates.
   */
  UPDATED,
  /**
   * When the state is NEEDS_UPDATE, proceed with further updates.
   */
  NEEDS_UPDATE,

  /**
   * When the state is CANT_BE_UPDATED, validation failed, and we can't continue with further updates.
   */
  CANT_BE_UPDATED;

}
