package at.avila.virtual.threads.controller;

/**
 * The type of lookup for the country.
 */
public enum CountryByType {
  /**
   * When the user provides the country id, then updates or deletes will be done using the id.
   */
  BY_ID,
  /**
   * When the user provides the country name, then updates or deletes will be done using the name.
   */
  BY_NAME
}
