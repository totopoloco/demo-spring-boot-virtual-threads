package at.avila.virtual.threads.controller;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import java.time.Instant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class HomeController {

  @GetMapping("/home")
  public String home() {
    return "home";
  }

  @PreDestroy
  public void destroy() {
    log.info("Destroying bean of type <{}> at <{}>.", this.getClass().getSimpleName(), Instant.now());
  }

  @PostConstruct
  public void postConstruct() {
    log.info("Constructing bean of type <{}> at <{}>", this.getClass().getSimpleName(), Instant.now());
  }

}
