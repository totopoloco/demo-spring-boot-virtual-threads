package at.avila.virtual.threads.controller;

import at.avila.virtual.threads.db.CountryService;
import at.avila.virtual.threads.model.Country;
import io.micrometer.observation.annotation.Observed;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@Observed
@CacheConfig(cacheNames = "myCountryItems")
public class CountryAdminController implements CountriesAdminApi {

  private final CountryService countryService;

  public CountryAdminController(CountryService countryService) {
    this.countryService = countryService;
  }


  @Override
  @CacheEvict(allEntries = true)
  public ResponseEntity<Void> deleteCountry(Optional<Country> country) {

    Optional<ResponseEntity<Void>> errorResponse = validateParameter(country);
    if (errorResponse.isPresent()) {
      return errorResponse.get();
    }
    this.countryService.delete(country.flatMap(Country::getId).get());
    return ResponseEntity.noContent().build();
  }

  private static Optional<ResponseEntity<Void>> validateParameter(Optional<Country> country) {

    if (country.isEmpty()) {
      return Optional.of(ResponseEntity.badRequest().build());
    }

    if (country.get().getId().isEmpty()) {
      return Optional.of(ResponseEntity.badRequest().build());
    }

    return Optional.empty();
  }

  @Override
  @CacheEvict(allEntries = true)
  public ResponseEntity<Country> saveCountry(@RequestBody Optional<Country> country) {

    return country.map(value -> ResponseEntity.ok(this.countryService.save(value)))
        .orElseGet(() -> ResponseEntity.badRequest().build());

  }

  @Override
  @CacheEvict(allEntries = true)
  public ResponseEntity<Void> updateCountry(Optional<Country> country) {
    return country
        .filter(value -> (tryToUpdateCountry(value) != DatabaseState.CANT_BE_UPDATED))
        .<ResponseEntity<Void>>
            map(
            value -> ResponseEntity.ok().build())
        .orElseGet(() -> ResponseEntity.badRequest().build());
  }

  private DatabaseState tryToUpdateCountry(Country c) {

    Optional<String> englishName = c.getEnglishName();
    if (englishName.isEmpty()) {
      return updateWithId(c);
    }

    Optional<Country> countryByName = this.countryService.getCountryByName(englishName.get());

    if (countryByName.isPresent()) {
      this.countryService.update(c, CountryByType.BY_NAME);
      return DatabaseState.UPDATED;
    }

    return DatabaseState.CANT_BE_UPDATED;
  }

  /**
   * Try to update with id.
   *
   * @param c the country.
   * @return the database state.
   */
  private DatabaseState updateWithId(Country c) {
    //Try one more time with id.
    Optional<Integer> id = c.getId();

    if (id.isEmpty()) {
      return DatabaseState.CANT_BE_UPDATED;
    }

    Optional<Country> countryById = this.countryService.getCountryById(id.get());
    if (countryById.isEmpty()) {
      return DatabaseState.CANT_BE_UPDATED;
    }

    this.countryService.update(c, CountryByType.BY_ID);
    return DatabaseState.UPDATED;
  }
}
