package at.avila.virtual.threads.controller;

import at.avila.virtual.threads.db.LanguageCountryService;
import at.avila.virtual.threads.model.Language;
import io.micrometer.observation.annotation.Observed;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@Observed
@CacheConfig(cacheNames = "myCountryItems")
public class LanguageCountryAdminController implements LanguageCountriesAdminApi {

  private final LanguageCountryService languageCountryService;

  public LanguageCountryAdminController(LanguageCountryService languageCountryService) {
    this.languageCountryService = languageCountryService;
  }

  @Override
  @CacheEvict(allEntries = true)
  public ResponseEntity<Void> updateLanguage(final Optional<Language> optionalLanguage) {

    if (optionalLanguage.isEmpty()) {
      return ResponseEntity.badRequest().build();
    }

    final Language language = optionalLanguage.get();
    final Optional<ResponseEntity<Void>> invalidFieldsOrOk = validateFields(language);

    if (invalidFieldsOrOk.isPresent()) {
      return invalidFieldsOrOk.get();
    }

    Optional<Language> optionalLanguageSaved = this.languageCountryService.updateLanguage(language);

    if (optionalLanguageSaved.isEmpty()) {
      return ResponseEntity.notFound().build();
    }

    return ResponseEntity.ok().build();
  }

  private Optional<ResponseEntity<Void>> validateFields(Language language) {
    if (language.getId().isEmpty()) {
      return Optional.of(ResponseEntity.badRequest().build());
    }

    if (language.getEnglishName().isBlank()) {
      return Optional.of(ResponseEntity.badRequest().build());
    }

    return Optional.empty();
  }
}
