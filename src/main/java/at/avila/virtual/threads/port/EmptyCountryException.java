package at.avila.virtual.threads.port;

public class EmptyCountryException extends RuntimeException {
  public EmptyCountryException() {
    super();
  }

  public EmptyCountryException(String message) {
    super(message);
  }

  public EmptyCountryException(String message, Throwable cause) {
    super(message, cause);
  }

  public EmptyCountryException(Throwable cause) {
    super(cause);
  }

  protected EmptyCountryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
