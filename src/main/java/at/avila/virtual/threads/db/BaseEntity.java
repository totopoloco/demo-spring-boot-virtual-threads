package at.avila.virtual.threads.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PostLoad;
import jakarta.persistence.PostPersist;
import jakarta.persistence.Transient;
import jakarta.persistence.Version;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Persistable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@EntityListeners(value = {AuditingEntityListener.class})
@MappedSuperclass
@Getter
@Setter
public abstract class BaseEntity<T extends Serializable> implements Persistable<T>, Serializable {

  @Serial
  private static final long serialVersionUID = 7895951637149514024L;

  @Id
  @GeneratedValue(strategy = jakarta.persistence.GenerationType.IDENTITY)
  @Getter(AccessLevel.NONE)
  protected Integer id;

  @CreatedBy
  @JsonIgnore
  protected String createdBy;

  @LastModifiedBy
  @JsonIgnore
  protected String lastModifiedBy;

  @CreatedDate
  @JsonIgnore
  protected LocalDateTime createdDate;

  @LastModifiedDate
  @JsonIgnore
  protected LocalDateTime lastModifiedDate;

  @Version
  private Long version;


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    BaseEntity<?> that = (BaseEntity<?>) o;

    return new EqualsBuilder().append(this.getId(), that.getId()).isEquals();
  }

  @Override
  public int hashCode() {
    return new
        HashCodeBuilder(17, 37)
        .append(this.getId())
        .toHashCode();
  }

  @Transient
  private boolean isNew = true;

  @Override
  public boolean isNew() {
    return this.isNew;
  }

  @PostLoad
  @PostPersist
  public void markNotNew() {
    this.isNew = false;
  }
}