package at.avila.virtual.threads.db;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import java.io.Serial;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity(name = "country")
@Table(name = "country")
@Getter
@Setter
public class Country extends BaseEntity<Integer> {

  @Serial
  private static final long serialVersionUID = 8875614120341227939L;

  @Column(
      name = "english_name",
      nullable = false,
      length = 100,
      unique = true
  )
  @JsonProperty("english_name")
  private String englishName;

  @OneToMany(mappedBy = "country", cascade = CascadeType.ALL, orphanRemoval = true)
  @JsonManagedReference
  @OrderBy("englishName ASC")
  private Set<CountryLanguage> languages;

  @Override
  public Integer getId() {
    return this.id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o instanceof Country that) {
      return new EqualsBuilder()
          .append(this.getId(), that.getId())
          .append(this.getEnglishName(), that.getEnglishName()).isEquals();
    }

    return false;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(this.getId())
        .append(this.getEnglishName()).toHashCode();
  }
}