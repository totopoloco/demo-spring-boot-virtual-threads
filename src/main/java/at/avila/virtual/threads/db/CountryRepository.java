package at.avila.virtual.threads.db;

import io.swagger.v3.oas.annotations.Hidden;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository for countries.
 * <p>
 * This is a Spring Data JPA repository.
 * </p
 */
@Hidden
public interface CountryRepository extends JpaRepository<Country, Integer> {

  /**
   * Find a country by its english name.
   *
   * @param englishName the english name.
   * @return an Optional of the country.
   */
  @Query(" from country c where c.englishName = :englishName ")
  Optional<Country> findByEnglishName(@Param("englishName") final String englishName);

  /**
   * Find all countries ordered by id descending.
   *
   * @return the list of countries.
   */
  @Query(" FROM country c ORDER BY c.id desc ")
  List<Country> findAllOrderByIdDesc();
}