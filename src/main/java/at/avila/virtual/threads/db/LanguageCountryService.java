package at.avila.virtual.threads.db;

import at.avila.virtual.threads.conversion.ConversionUtility;
import at.avila.virtual.threads.model.Language;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
@Slf4j
public class LanguageCountryService {

  private final CountryLanguageRepository countryLanguageRepository;

  public LanguageCountryService(CountryLanguageRepository countryLanguageRepository) {
    this.countryLanguageRepository = countryLanguageRepository;
  }

  /**
   * Update a language.
   *
   * @param language the language.
   * @return the updated language.
   */
  public Optional<Language> updateLanguage(final Language language) {
    log.info("Updating language");


    Optional<Integer> id = language.getId();
    if (id.isEmpty()) {
      return Optional.empty();
    }

    Optional<CountryLanguage> byId = this.countryLanguageRepository.findById(id.get());
    if (byId.isEmpty()) {
      return Optional.empty();
    }

    final CountryLanguage countryLanguage = byId.get();
    countryLanguage.setEnglishName(language.getEnglishName());
    return Optional.of(ConversionUtility.convertFromDbToApi(this.countryLanguageRepository.save(countryLanguage)));

  }
}
