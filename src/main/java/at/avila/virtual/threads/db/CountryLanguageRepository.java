package at.avila.virtual.threads.db;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;

@Hidden
public interface CountryLanguageRepository extends JpaRepository<CountryLanguage, Integer> {

}