package at.avila.virtual.threads.db;

import at.avila.virtual.threads.controller.CountryByType;
import at.avila.virtual.threads.conversion.ConversionUtility;
import at.avila.virtual.threads.model.Language;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for countries.
 */
@Component
@Transactional
@Slf4j
public class CountryService {
  private final CountryRepository countryRepository;
  private final CountryLanguageRepository countryLanguageRepository;

  /**
   * Constructor.
   *
   * @param countryRepository         the country repository
   * @param countryLanguageRepository the country language repository
   */
  public CountryService(final CountryRepository countryRepository,
                        final CountryLanguageRepository countryLanguageRepository) {
    this.countryRepository = countryRepository;
    this.countryLanguageRepository = countryLanguageRepository;
  }

  /**
   * Get all countries.
   *
   * @return the list of countries
   */
  public List<at.avila.virtual.threads.model.Country> getAll() {

    return this.countryRepository.findAllOrderByIdDesc()
        .stream()
        .map(ConversionUtility::convertFromDbToApi)
        .toList();

  }

  /**
   * Get a country by its name.
   *
   * @param name the name of the country
   * @return the country
   */
  public Optional<at.avila.virtual.threads.model.Country> getCountryByName(final String name) {
    return this.countryRepository.findByEnglishName(name).map(ConversionUtility::convertFromDbToApi);
  }

  private Optional<Country> getCountryByIdRaw(final Integer id) {
    return this.countryRepository.findById(id);
  }

  public Optional<at.avila.virtual.threads.model.Country> getCountryById(final Integer id) {
    return this.getCountryByIdRaw(id).map(ConversionUtility::convertFromDbToApi);
  }

  /**
   * Save a country if it does not exist yet.
   * If it exists, return the existing one.
   *
   * @param country the country to save
   * @return the saved country
   */
  public at.avila.virtual.threads.model.Country save(final at.avila.virtual.threads.model.Country country) {
    Country countryConverted = ConversionUtility.convertFromApiToDb(country);
    Country countryInserted =
        findByEnglishName(countryConverted.getEnglishName()).orElseGet(() -> this.countryRepository.save(countryConverted));
    return ConversionUtility.convertFromDbToApi(countryInserted);
  }

  /**
   * Update a country.
   * If it does not exist, return an empty optional.
   *
   * @param country the country to update
   * @param byId
   */
  public void update(final at.avila.virtual.threads.model.Country country, CountryByType byId) {

    if (Objects.isNull(country)) {
      log.warn("Country is null");
      return;
    }
    updateCountry(country, byId);
  }

  private void updateCountry(at.avila.virtual.threads.model.Country country, CountryByType byId) {
    getCountry(country, byId)
        .ifPresent(countryByEnglishName -> {
          final Set<CountryLanguage> existingLanguages = countryByEnglishName.getLanguages();
          country.getLanguages()
              .stream()
              .filter(language -> checkNewLanguageNotInDB(language, existingLanguages))
              .forEach(language -> {
                final CountryLanguage countryLanguage = new CountryLanguage();
                countryLanguage.setEnglishName(language.getEnglishName());
                countryLanguage.setCountry(countryByEnglishName);
                this.countryLanguageRepository.save(countryLanguage);
              });
        });
  }

  private static boolean checkNewLanguageNotInDB(final Language language,
                                                 final Set<CountryLanguage> existingLanguages) {
    return existingLanguages
        .stream()
        .noneMatch(existingLanguage ->
            existingLanguage.getEnglishName().equals(language.getEnglishName())
        );
  }

  private Optional<Country> getCountry(at.avila.virtual.threads.model.Country country, CountryByType byId) {

    if (byId == CountryByType.BY_ID) {
      return country.getId().flatMap(this.countryRepository::findById);
    }

    Optional<String> englishName = country.getEnglishName();
    if (englishName.isEmpty()) {
      return Optional.empty();
    }
    return findByEnglishName(englishName.get());

  }

  private Optional<Country> findByEnglishName(final String countryEnglishName) {
    return this.countryRepository.findByEnglishName(countryEnglishName);
  }

  public void delete(Integer id) {
    this.countryRepository.deleteById(id);
  }
}