package at.avila.virtual.threads.conversion;

import at.avila.virtual.threads.db.Country;
import at.avila.virtual.threads.db.CountryLanguage;
import at.avila.virtual.threads.model.Language;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public final class ConversionUtility {

  private ConversionUtility() {
    throw new UnsupportedOperationException("Forbidden to create instance of this class");
  }

  public static at.avila.virtual.threads.model.Country convertFromDbToApi(Country country) {
    at.avila.virtual.threads.model.Country countryOpenApi =
        new at.avila.virtual.threads.model.Country();
    countryOpenApi.setEnglishName(Optional.ofNullable(country.getEnglishName()));
    countryOpenApi.setId(Optional.ofNullable(country.getId()));
    countryOpenApi.setLanguages(mapDbLanguages(country));
    return countryOpenApi;
  }

  public static at.avila.virtual.threads.model.Language convertFromDbToApi(CountryLanguage countryLanguage) {
    at.avila.virtual.threads.model.Language language = new at.avila.virtual.threads.model.Language();
    language.setId(Optional.ofNullable(countryLanguage.getId()));
    language.setEnglishName(countryLanguage.getEnglishName());
    return language;
  }

  private static List<Language> mapDbLanguages(final Country country) {
    return country.getLanguages().stream().map(l -> {
          Language language = new Language(l.getEnglishName());
          language.setId(Optional.ofNullable(l.getId()));
          return language;
        })
        .toList();
  }

  public static Country convertFromApiToDb(at.avila.virtual.threads.model.Country country) {
    Country countryDb = new Country();
    country.getId().ifPresent(countryDb::setId);
    countryDb.setEnglishName(country.getEnglishName().orElseGet(() -> null));
    countryDb.setLanguages(mapApiLanguages(country, countryDb));
    return countryDb;
  }

  private static Set<CountryLanguage> mapApiLanguages(
      final at.avila.virtual.threads.model.Country country, Country countryDb) {
    return country.getLanguages().stream().map(language -> {
      CountryLanguage countryLanguage = new CountryLanguage();
      language.getId().ifPresent(countryLanguage::setId);
      countryLanguage.setEnglishName(language.getEnglishName());
      countryLanguage.setCountry(countryDb);
      return countryLanguage;
    }).collect(Collectors.toSet());

  }


}
