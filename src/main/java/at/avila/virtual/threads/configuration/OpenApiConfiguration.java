package at.avila.virtual.threads.configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
    info = @Info(
        contact = @Contact(
            name = "Marco Tulio Ávila Cerón",
            email = "marcotavilac@pm.me"
        ),
        description = "OpenAPI 3.0.3",
        title = "Open API specification for Demo concepts",
        license = @License(
            name = "MIT",
            url = "https://opensource.org/licenses/MIT"
        )
    ), servers = {
    @Server(description = "localhost - 8060", url = "http://localhost:8060"),
    @Server(description = "localhost - 8080", url = "http://localhost:8080")
}
)
public class OpenApiConfiguration {
}
