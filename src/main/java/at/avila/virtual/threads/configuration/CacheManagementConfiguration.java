package at.avila.virtual.threads.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableCaching
@EnableScheduling
@Getter
@Setter
public class CacheManagementConfiguration {
  @Value("${cache.clean.cron}")
  public String cleanCacheCron;
}
