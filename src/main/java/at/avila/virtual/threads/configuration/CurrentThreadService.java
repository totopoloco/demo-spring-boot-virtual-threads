package at.avila.virtual.threads.configuration;

import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CurrentThreadService {

  /**
   * Get current thread.
   *
   * @param currentThread current thread, in case of null {@link Thread#currentThread()} is used
   * @return current thread if not null, otherwise current thread from {@link Thread#currentThread()}
   */
  public String currentThread(final String currentThread) {

    String strCurrentThread = Optional.ofNullable(currentThread).orElseGet(() -> {
      log.warn("Current thread parameter is null");
      return Thread.currentThread().toString();
    });

    log.info("Current thread: {}", strCurrentThread);
    return strCurrentThread;
  }
}
