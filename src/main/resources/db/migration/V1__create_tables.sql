create table IF NOT EXISTS country
(
    ID                 int          not null AUTO_INCREMENT,
    ENGLISH_NAME       varchar(100) not null,
    CREATED_BY         varchar(100) null,
    CREATED_DATE       timestamp    null,
    LAST_MODIFIED_BY   varchar(100) null,
    LAST_MODIFIED_DATE timestamp    null,
    PRIMARY KEY (ID)
);