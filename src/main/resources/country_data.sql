INSERT INTO COUNTRY (ENGLISH_NAME, CREATED_BY, CREATED_DATE, VERSION)
VALUES ('India', 'mavila', current_timestamp(), 0);
INSERT INTO COUNTRY (ENGLISH_NAME, CREATED_BY, CREATED_DATE, VERSION)
VALUES ('Brazil', 'mavila', current_timestamp(), 0);
INSERT INTO COUNTRY (ENGLISH_NAME, CREATED_BY, CREATED_DATE, VERSION)
VALUES ('USA', 'mavila', current_timestamp(), 0);
INSERT INTO COUNTRY (ENGLISH_NAME, CREATED_BY, CREATED_DATE, VERSION)
VALUES ('Italy', 'mavila', current_timestamp(), 0);

INSERT into COUNTRY_LANGUAGE (ID_COUNTRY, ENGLISH_NAME, CREATED_BY, CREATED_DATE, VERSION)
VALUES (1, 'Hindi', 'mavila', current_timestamp(), 0);
INSERT into COUNTRY_LANGUAGE (ID_COUNTRY, ENGLISH_NAME, CREATED_BY, CREATED_DATE, VERSION)
VALUES (1, 'English', 'mavila', current_timestamp(), 0);
INSERT into COUNTRY_LANGUAGE (ID_COUNTRY, ENGLISH_NAME, CREATED_BY, CREATED_DATE, VERSION)
VALUES (2, 'Portuguese', 'mavila', current_timestamp(), 0);
INSERT into COUNTRY_LANGUAGE (ID_COUNTRY, ENGLISH_NAME, CREATED_BY, CREATED_DATE, VERSION)
VALUES (3, 'English', 'mavila', current_timestamp(), 0);
INSERT into COUNTRY_LANGUAGE (ID_COUNTRY, ENGLISH_NAME, CREATED_BY, CREATED_DATE, VERSION)
VALUES (4, 'Italian', 'mavila', current_timestamp(), 0);