package at.avila.virtual.threads.db;

import static org.assertj.core.api.Assertions.assertThat;

import at.avila.virtual.threads.conversion.ConversionUtility;
import at.avila.virtual.threads.model.Language;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;

@DataJpaTest
@TestPropertySource(locations = "classpath:application-test.properties")
class CountryServiceTest {

  @Autowired
  private TestEntityManager entityManager;

  @Autowired
  private CountryRepository countryRepository;

  @Autowired
  private CountryLanguageRepository countryLanguageRepository;

  private CountryService countryService;

  @BeforeEach
  void setUp() {
    this.countryService = new CountryService(countryRepository, countryLanguageRepository);

    Country country = new Country();
    country.setEnglishName("Test Country");

    CountryLanguage language = new CountryLanguage();
    language.setEnglishName("Test Language");
    language.setCountry(country);

    Set<CountryLanguage> countryLanguages = new HashSet<>();
    countryLanguages.add(language);
    country.setLanguages(countryLanguages);

    this.entityManager.persist(country);
    this.entityManager.flush();
  }

  @Test
  void whenFindAll_thenReturnCountries() {
    List<at.avila.virtual.threads.model.Country> found = this.countryService.getAll();

    assertThat(found).isNotEmpty();
    Optional<String> englishName = found.getFirst().getEnglishName();
    assertThat(englishName).isNotEmpty().contains("Test Country");
    assertThat(found.getFirst().getLanguages().getFirst().getEnglishName()).isEqualTo("Test Language");
  }

  @Test
  void whenFindCountryByName_thenReturnsCountry() {
    Optional<at.avila.virtual.threads.model.Country> found = this.countryService.getCountryByName("Test Country");

    assertThat(found).isNotEmpty();
    assertThat(found.get().getEnglishName()).isNotEmpty().contains("Test Country");
    List<Language> languages = found.get().getLanguages();
    Optional<String> first = languages
        .stream()
        .map(Language::getEnglishName)
        .findFirst();
    assertThat(first).isNotEmpty().contains("Test Language");
  }

  @Test
  void whenSaveCountry_thenReturnsCountry() {
    Country country = new Country();
    country.setEnglishName("Test Country 2");

    CountryLanguage language = new CountryLanguage();
    language.setEnglishName("Test Language 2");
    language.setCountry(country);

    country.setLanguages(Set.of(language));

    at.avila.virtual.threads.model.Country countryConverted = ConversionUtility.convertFromDbToApi(country);
    at.avila.virtual.threads.model.Country saved = this.countryService.save(countryConverted);

    assertThat(saved).isNotNull();
    Optional<String> englishName = saved.getEnglishName();
    assertThat(englishName).isNotEmpty().contains("Test Country 2");
    assertThat(saved.getLanguages().getFirst().getEnglishName()).isEqualTo("Test Language 2");
  }

  @Test
  void whenUpdateCountry_thenReturnsCountry() {
    final Country paraguay = new Country();
    paraguay.setEnglishName("Paraguay");
    final CountryLanguage spanish = new CountryLanguage();
    spanish.setEnglishName("Spanish");
    spanish.setCountry(paraguay);
    final Set<CountryLanguage> countryLanguageList = new HashSet<>();
    countryLanguageList.add(spanish);
    paraguay.setLanguages(countryLanguageList);
    final Country savedParaguay = this.countryRepository.save(paraguay);
    assertThat(savedParaguay.getLanguages()).isNotEmpty().hasSize(1);

    final CountryLanguage guarani = new CountryLanguage();
    guarani.setEnglishName("Guarani");
    guarani.setCountry(savedParaguay);
    Set<CountryLanguage> languages = savedParaguay.getLanguages();
    languages.add(guarani);
    savedParaguay.setLanguages(languages);

    final Country updated = this.countryRepository.save(savedParaguay);
    assertThat(updated.getLanguages()).isNotEmpty().hasSize(2);

    final CountryLanguage guaraniStubborn = new CountryLanguage();
    guaraniStubborn.setEnglishName("Guarani");
    guaraniStubborn.setCountry(updated);
    Set<CountryLanguage> languagesAfterFirstUpdate = updated.getLanguages();
    languagesAfterFirstUpdate.add(guaraniStubborn);
    updated.setLanguages(languagesAfterFirstUpdate);

    final Country updatedPossible = this.countryRepository.save(updated);

    assertThat(updatedPossible.getLanguages()).isNotEmpty().hasSize(2);

  }

  @Test
  void whenDeleteCountry_thenReturnsCountry() {
    final Country ireland = new Country();
    ireland.setEnglishName("Ireland");
    final CountryLanguage english = new CountryLanguage();
    english.setEnglishName("English");
    english.setCountry(ireland);
    final Set<CountryLanguage> countryLanguageList = new HashSet<>();
    countryLanguageList.add(english);
    ireland.setLanguages(countryLanguageList);
    final Country savedIreland = this.countryRepository.save(ireland);
    assertThat(savedIreland.getLanguages()).isNotEmpty().hasSize(1);

    final CountryLanguage gaelic = new CountryLanguage();
    gaelic.setEnglishName("Gaelic");
    gaelic.setCountry(savedIreland);
    Set<CountryLanguage> languages = savedIreland.getLanguages();
    languages.add(gaelic);
    savedIreland.setLanguages(languages);
    final Country updated = this.countryRepository.save(savedIreland);
    assertThat(updated.getLanguages()).isNotEmpty().hasSize(2);

    this.countryRepository.delete(updated);

    Optional<Country> ireland1 = this.countryRepository.findByEnglishName("Ireland");
    assertThat(ireland1).isEmpty();

  }

}