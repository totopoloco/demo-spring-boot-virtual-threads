# Prerequisites

* Verify the Java installation is the correct, for this project it is Java 21
 - Verify JAVA_HOME points to Java 21 and PATH 
 ```
 java --version
 ```
 displays the correct version

  - See https://adoptium.net/temurin/releases/

* Verify as well Docker Desktop is running
  - See https://www.docker.com/products/docker-desktop/

* Command curl is available in the system
  - See https://curl.se/windows/microsoft.html


# Prepare your environment
* Verify that you can checkout code from Gitlab (the same way we do it for GitHub)
 
 ```
 eval "$(ssh-agent -s)"
 ssh-add ~/.ssh/[your key]
 ssh-add -l
 ```

 * Checkout code as mentioned in the README.md
 ```
 git clone git@gitlab.com:totopoloco/demo-spring-boot-concepts.git
 ```

 * Launch the services in Docker
 - INFO: In Powershell
 ```
 cd observability
 docker compose up -d
 ```

 If everything goes fine we see docker desktop is running our service or:
 ```
 docker ps
 ```
 
 # Extra configuration
 
 ## Form based authentication
 Great effort has been put to make it run out of the box, however Keycloak needs to be tweaked before.
 - Go to http://localhost:8080
 - Click in "Administration Console"
 - The initial login and password is
 	- admin
 	- keycloak
 - In the upper left in the menu for the realms, change it to oauth2-demo-realm
 - Click in Clients
 - In the column Client ID click in "oauth2-demo-thymeleaf-client"
 - Verify that Valid redirect URIs is set to http://localhost:8060/login/oauth2/code/oauth2-demo-thymeleaf-client
 - Go to the tab "Credentials"
 - in "Client Secret" click the button "Regenerate"
 - Copy the secret, e.g. tk2E1VX4MdSMWszD08U2J9AXMEs3yG5t
 - Go back to "Settings" tab and scroll down to "Authentication flow" verify that Direct access grants is not checked
   - We will need to check it if we use JWT token based authentication.

 Create a user
 - On the left menu click in "Users"
 - And click in "Add user"
 - Username type whatever username you wish, e.g. mavila
 - Click "create"
 - The user details page will be shown
 - Click in the tab "Credentials"
 - Click in "Set password"
 - Click whatever password e.g. mavila123
 - Leave "Temporary" as "On"
 - Click "Save"

 Configure the properties in application.properties from the Spring Boot application.
 This file is locate under
 
 ```
  [workarea]\demo-spring-boot-concepts\src\main\resources
 ```

 In the "Security" section change the property 
 ````
 spring.security.oauth2.client.registration.oauth2-demo-thymeleaf-client.client-secret=[secret]
 ````
 to e.g. the secret from above section
 ```
 spring.security.oauth2.client.registration.oauth2-demo-thymeleaf-client.client-secret=tk2E1VX4MdSMWszD08U2J9AXMEs3yG5t
 ```
 
 # Time to test our form based authentication!
 Launch the Spring Boot application in Powershell (looks very much like a Linux shell)
  ```
  .\gradlew bootRun
  ```
  - If everything goes fine with a web browser navigate to http://localhost:8060/home
  - a login and password prompt will be shown (Keycloak)
  - type the defined user in above step
    - mavila
    - mavila123
  - Then change to whatever password you desire
  - The page with the banner "Home. Spring Boot + Thymeleaf Example" is shown.
  - As well try http://localhost:8060/

 Api docs
  - http://localhost:8060/api/swagger-ui/index.html
  - Try countries fetch
  - As it is authenticated we can access the API