# demo-spring-boot-concepts



## Introduction

The aim of this project is to explore different concepts within the Spring ecosystem.
Starting with a Spring Boot application, this project comprises as of now of the following tecnologies

- [ ] Virtual threads
- [ ] Observability
- [ ] Rest Services
- [ ] JPA (using mySQL and H2 for tests)
- [ ] Flyway for easy database setup
- [ ] OpenAPI and Swagger for API generation

## Requirements

- [ ] This project is intended to use the latest Spring Boot version and the latest Java version as of 01.2024
- [ ] Please in your local environment install [Java 21](https://openjdk.org/projects/jdk/21/)
- [ ] This project will compile with Spring Boot 3.2.1 in the configuration


## Work with this project

- [ ] Checkout this project, in this example with SSH:

```
git clone git@gitlab.com:totopoloco/demo-spring-boot-concepts.git
```

## Docker compose

- [ ] Assuming Docker / Docker Desktop has been installed already, otherwise please follow the instruction installing Docker for your platform:
- [ ] [Get started with Docker Desktop](https://www.docker.com/products/docker-desktop/)
- [ ] Launch the services:

```
cd observability
docker compose up -d
```

## Start the app

- [ ] This is a Spring Boot application, it can be started in different ways
- [ ] IDEA Intellij. Open the project, and execute the Gradle task "bootRun"
- [ ] From the command line:

- [ ] In UNIX environments
```
$ ./gradlew bootRun
```

- [ ] In Windows environments

```
> gradlew.bat bootRun
```